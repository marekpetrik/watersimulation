#!/bin/python 
"""
Created on Thu Feb 14 15:39:17 2019

@author: Shannon
"""


#### GW Model
import scipy.optimize as so
import numpy as np
import pandas as pd
import csv
import dateutil
import datetime
from itertools import islice
from math import isnan, log, exp


""" 
Runs the optimization for a particular parcel

@param data Rainfall data
@param parcel_data Home information (each house is a single row)
@param home_index Index of the house that should be processed
"""
def optimize(data, parcel_data, home_index):
    # conversion to square meters
    roof_area= 0.092903*parcel_data['LIVING_AREA'][home_index]/parcel_data['NUM_FLOORS'][home_index]
    lawn_area = parcel_data['LAND_SF'][home_index]-roof_area
    
    #bedrooms
    
    R_BDRMS = pd.Series(parcel_data['R_BDRMS']).astype(int)[home_index]
    U_BDRMS = pd.Series(parcel_data['U_BDRMS']).astype(int)[home_index]
    
    bedrooms = R_BDRMS + U_BDRMS
        
    #population
    population = 4771936
    boston_bdrms=1930177
    tenants_per_bedroom = population/boston_bdrms
    
    
    number_of_tenants_per_fixture= 2  # tenants
    tenant_number = bedrooms*tenants_per_bedroom
    fixtures= tenant_number / number_of_tenants_per_fixture  #number of toilets
    
    
    number_of_floors = parcel_data['NUM_FLOORS'][home_index]
    building_height= np.maximum(3*(np.array(number_of_floors)-1), 1.5)
    
    # -- Used Flow for Reuse
    loads_per_day = 0.32         ##laundry loads/person/day
    m3_per_load_per_day = 0.0567812     # m3 of water per load of laundry
    m3_per_shower_per_day = 0.0651091   # m3/day
    m3_sink_per_day = 0.0227125         # m3/day
    total_influent = tenant_number *((m3_per_load_per_day*loads_per_day) + m3_per_shower_per_day + m3_sink_per_day)
                        #m3/day
        
    electricity_price = 0.216  #$/kWh
    water_density=1000  #kg/m3
    pump_efficiency = 0.5  #dimensionless
    rate = 4.23                              #$/m^3 water and wastewater price from public supply
    tank_material = 0                     #type of material, 0 denotes plastic and 1 denotes steel
    fixture_rate = 0.072                 #m3/person/day rate of toilet flushing
    flushing=tenant_number*fixture_rate  #m3/day daily total flushing 
    
    ETo=0.06 #inch/day evapotranspiration in Boston
    PF=0.8  #plant factor, dimensionless
    efficiency = 0.8
    irrigation=ETo*PF*lawn_area*0.62*0.00378541/efficiency  #m3/day
    
    energy_conversion_factor= 3.6 #MJ/kWh
    treatment_energy_intensity = (0.2255-0.19) #MJ/m3
    friction_loss_coefficient = 0.8 #m/km
    foot_of_head_conversion = 2.31 
    plant_elevation = 93 #m
    home_elevation = parcel_data['Avg_ELEV'][home_index] #m
    central_pump_efficiency = 0.7
    distance_from_plant = (parcel_data['Dist_DW']+17000)[home_index]/1000 #km
    operating_pressure = 27.5 #psi
    maint_energy_intensity =  6.76 #MJ/$
    pump_embodied_energy_intensity = 8.49 #MJ/$
    tank_embodied_energy_intensity = 14.8 #MJ/$
    installation_energy_intensity = 6.21 #MJ/$
    velocity = 2 #m/s
    dynamic_pressure=0.5*velocity**2/9.81        #m
    dailydiscount = exp(log(1.03)/365) - 1
    
    # ---- Wastewater Treatment Savings
    distance_from_wwtp = pd.Series(parcel_data['DistWWTP'])[home_index]/1000 #km
    wwtp_elevation = 0                  #km
    wwtp_treatment_energy_intensity = 2 #MJ/m3
    
    def simulate(tank_size, output = "cost"):
        """
        Simulates the household.
            
        Returns the negative cost savings and negative energy savings
        """
        if tank_material == 0:
            tank_cost = 358.77*(tank_size**0.5064)
        else: tank_cost = 1292.1*tank_size**0.6771
        
        pump_size_hp = max(0.5,(fixture_rate*fixtures*water_density*9.8)*(building_height/2)/745.7/86400/pump_efficiency)
        
       
        pump_cost = -31.2*max(pump_size_hp, 7)**2 + \
                     486.33*max(pump_size_hp, 7) + 205.68
        
        installation_cost = tank_cost * 0.6 #$
        annual_maint_cost = 100  #$ annual maintenance cost
        total_maint_cost = annual_maint_cost*30 #$ total maintenance cost over 30 years of life span
        system_base_cost = installation_cost + pump_cost + tank_cost + total_maint_cost
        pumping_energy_cost_cum = 0 #$
        cost_saving_cum = 0 #$
        outflow_cum = 0 #m3
        storage = 0  #m3 initial storage in the tank
        treatment_energy_cum = 0
        wwtp_treatment_energy_cum = 0
        centralized_pumping_energy_cum = 0 #kWh
        avoided_pumping_energy_cum = 0
        wwtp_pumping_energy_cum = 0
        pumping_energy_cum = 0
        total_maint_energy = total_maint_cost * maint_energy_intensity #MJ
        pump_manufacture_energy = pump_embodied_energy_intensity * pump_cost #MJ
        tank_manufacture_energy = tank_embodied_energy_intensity * tank_cost #MJ
        installation_energy = installation_energy_intensity * installation_cost #MJ
        construction_energy = pump_manufacture_energy + tank_manufacture_energy + installation_energy #MJ
        
        t = 0 #date
        for (precipitation, date) in zip(precipitations, dates):
            t += 1
            date_parsed = dateutil.parser.parse(date)
            
            if date_parsed.month >= 5 and date_parsed.month <= 9 and precipitation == 0:
                demand = irrigation + flushing #evapotranspiration
            else:
                demand = flushing
            capacity = tank_size-storage
            storage = np.minimum(storage+total_influent, tank_size)
            outflow = np.minimum(demand, storage)
            outflow_cum += outflow
            publicsupply = demand - outflow  
            storage -= outflow 
            publicsupply_cost = publicsupply*rate
            cost_saving = outflow*rate #$
            cost_saving_cum += cost_saving / (1 + dailydiscount)**t #$
            treatment_energy = treatment_energy_intensity * outflow #(MJ/Day)
            treatment_energy_cum += treatment_energy
            wwtp_treatment_energy = wwtp_treatment_energy_intensity * outflow
            wwtp_treatment_energy_cum += wwtp_treatment_energy
            elevation_change = home_elevation - plant_elevation #m
            wwtp_elevation_change = wwtp_elevation - home_elevation         #m
            operating_head = foot_of_head_conversion * operating_pressure * 0.3048 #m
            friction_loss = distance_from_plant * friction_loss_coefficient #(m)
            wwtp_friction_loss= distance_from_wwtp * friction_loss_coefficient      #m
            total_head = elevation_change + friction_loss + operating_head+ dynamic_pressure  #(m)
            wwtp_total_head = wwtp_elevation_change + wwtp_friction_loss   #m
            centralized_pumping_energy = outflow * water_density * 9.8 * total_head / central_pump_efficiency / 3600000 #(kWh)
            centralized_pumping_energy_cum += centralized_pumping_energy #kWh
            wwtp_pumping_energy = outflow * water_density * 9.8 * wwtp_total_head / central_pump_efficiency / 3600000 #(kWh)
            wwtp_pumping_energy_cum += wwtp_pumping_energy
            avoided_pumping_energy = (centralized_pumping_energy + wwtp_pumping_energy) * energy_conversion_factor \
                                     + treatment_energy + wwtp_treatment_energy #MJ
            avoided_pumping_energy_cum += avoided_pumping_energy #MJ
    
            
            # add a fudge factor to the denominator to make sure that we are not dividing by zero
            indoor_use_ratio = flushing / max(0.001, demand)  #ratio of toilet flushing over total demand
            pumping_energy = outflow*indoor_use_ratio*water_density*9.8*(building_height/2)*.000000278/pump_efficiency  #kWh/day
            pumping_energy_cost = electricity_price * pumping_energy #$
            pumping_energy_cost_cum += pumping_energy_cost  / (1 + dailydiscount)**t #$
            pumping_energy_cum += pumping_energy #kWh
            
        operation_energy = (pumping_energy_cum * energy_conversion_factor*2.26)  \
                            + total_maint_energy #MJ
        net_embodied_energy_saving = (avoided_pumping_energy_cum*2.26)  \
                            - construction_energy \
                            - operation_energy #MJ
        
        net_saving = cost_saving_cum - system_base_cost - pumping_energy_cost_cum - system_base_cost*0.08    #$
            
        if output == "energy":
            assert (not isnan(net_embodied_energy_saving))
            return - net_embodied_energy_saving
        elif output == "cost":
            net_saving = cost_saving_cum - system_base_cost * 1.08 \
                        - pumping_energy_cost_cum  #$
            assert (not isnan(net_saving))
            return - net_saving
        else:
            assert False
            
    # Runs minimization of costs
    print("optimizing costs")
    opt_c = so.minimize_scalar(simulate, method='bounded', bounds=(0,40), tol=0.1, options={'maxiter':20, 'disp':3})
    print("Costs: Optimal tank size: ", opt_c.x, " savings: ", -opt_c.fun)
        
    # Runs minimization of energy
        
    print("Optimizing energy")
    opt_e = so.minimize_scalar(lambda x: simulate(x, "energy"), method='bounded', bounds=(0,40), tol=0.1, options={'maxiter':50, 'disp':3})
    print("Energy: Optimal tank size: ", opt_e.x, " savings: ", -opt_e.fun)
        
    return {"Zipcode" : parcel_data['Zipcode'][home_index],
            "ST_NAME" : parcel_data['ST_NAME'][home_index],
            "ST_NUM" : parcel_data['ST_NUM'][home_index],
            "opt_costs_size": opt_c.x,
            "opt_cost": -opt_c.fun,
            "opt_energy_size" : opt_e.x,
            "opt_energy" : -opt_e.fun }

## Load data

if __name__ == "__main__":
    # home information
    parcel_data = pd.read_csv("../merged/all.csv")
    # precipitation data
    data = pd.read_csv ("Boston_Rainfall.csv")
    precipitations = data['Precipitation']  #mm
    dates = data['Date']
    
## Process data (seq)
#if __name__ == "__main__":
#    for home_index in range(parcel_data.size): # which one of the homes to compute the values for
#        optimize(data, parcel_data, home_index)
        
## Process data (Parallel)

from multiprocessing import Pool
from os import cpu_count
import pickle
import time

def run_index(index):
    return optimize(data, parcel_data, index)

if __name__ == "__main__":
    start_time = time.time()

    pool = Pool(cpu_count())
    print("cpu count", cpu_count())
    print("entries", parcel_data.shape[0])
    output = pool.imap_unordered(run_index, list(range(parcel_data.shape[0])))
    output_list = []
    for x in output:
        output_list.append(x)
        print("**********************************************")
        print("appending", len(output_list), "out of", parcel_data.shape[0], "time", (time.time() - start_time) )
        print("**********************************************")
    
    # save the output
    with open('output.pickle', 'wb') as f:
        pickle.dump(output_list, f, pickle.HIGHEST_PROTOCOL)
     
    output_frame = pd.DataFrame.from_dict(output_list)
    output_frame.to_csv("simulated_gw.csv")
